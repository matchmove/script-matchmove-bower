module.exports = (function () {

    'use strict';
    
    var _settings = {
        library: '#captcha-library',
        container: '#captcha-container',
        key: null,
        className: {
            hidden: 'displayNone'
        }
    };
    
    var self = {}, Captcha, $;
    
    self.init = function (_$, _Captcha, options) {
        
        $ = _$;
        Captcha = _Captcha;
        
        _settings = options = $.extend({}, _settings, options);
        
        var library = $(options.library);
        
        if (!library.length) {
            console.log(options.library + ' must be loaded using <script>. ref: https://developers.google.com/recaptcha/docs/display#AJAX');
            return false;
        }
        
        var src = (library.attr('src') || '').split('#');
        
        if (2 > src.length) {
            console.log(options.library + ' does not contain the public key which must be separated by a #.');
            return false;
        }
        
        _settings.key = src[1];
        
        return self;
    };
    
    self.settings = function () {
        return _settings;
    };
    
    self.Captcha = function () {
        return Captcha;
    }
    
    self.getData = function () {
        return {
            challenge: self.Captcha().get_challenge(),
            response: self.Captcha().get_response()
        };
    }
    
    self.isCreated = function () {
        return '' != $(self.settings().container).html();
    };
    
    self.create = function (config) {
        var captcha = self.Captcha(),
            options = self.settings(),
            container = $(options.container);
        
        return captcha.create(options.key, container.attr('id'), config);
    }
    
    self.reload = function () {
        self.Captcha().reload();
        return self.Captcha();
    }
    
    return self;
}());