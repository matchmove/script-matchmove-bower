module.exports = (function () {

    'use strict';
    
    /**
     * Standardizes XHRequests by adding status messages.
     */
    var $, Messages, _settings = {
        type: 'POST',
        url: '',
        data: {},
        timeout: 60000,
        dataType: 'json',
        
        selectors: {
            netError: '.netError'
        }
    };
    
    var _callbacks = {
        
        fail: function (XMLHttpRequest, textStatus, errorThrown) {
            switch (errorThrown) {
                //error 500
                case 'Internal Server Error':
                    Messages.error(errorThrown);
                    break;
    
                case 'timeout':
                    _settings.selectors.netError.show();
                    break;
            }
        }
    
    };

    /**
     * Sends an XHRequest with a built-in messaging display.
     *
     * @param       options     jQuery.ajax options.
     * @returns     jQuery.ajax
     */
    var self = function (options) {
        options = $.extend({}, _settings, options);
        
        var ajax = $.ajax(options);
        
        $.each(_callbacks, function (key, value) {
            ajax[key](value);
        });
        
        return ajax;
    };
    
    self.init = function (_$, _Messages) {
        $ = _$;
        Messages = _Messages;
        
        return self;
    };
    
    return self;
}());