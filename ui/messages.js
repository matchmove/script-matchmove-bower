module.exports = (function () {

    'use strict';
        
    /**
     * Displays a proper status message.
     */
    var self = {}, $;

    /**
     * Initializes the Messages Object
     *
     * @param       options     Settings.
     */
    self.init = function (_$, options) {
        $ = _$;

        self.settings = {
            target:  $('div.error-message'),
            
            selectors: {
                text: '.media .media-body'
            },
            
            className: {
                success: 'message--success',
                error: 'message--error',
                warning: 'message--warning',
                hidden: 'displayNone'
            }
            
        };

        $.extend(self.settings, options);
        
        return self;
    };

    /**
     * Returns the object where the status message will be inserted.
     */
    self.getTextObject = function () {
        return self.settings.target.find(self.settings.selectors.text);
    };

    /**
     * Displays the status message.
     *
     * @param       message     Status message to be displayed.
     * @returns     Messages.settings.target
     */
    self.show = function (message) {
        var settings = self.settings;
        
        self.getTextObject().html(message);
        
        return settings.target.removeClass([
            settings.className.hidden,
            settings.className.success,
            settings.className.warning,
            settings.className.error
        ].join(' '));
    };
    
    /**
     *  Hides the status message.
     */
    self.hide = function () {
        var settings = self.settings;
        return settings.target.addClass(settings.className.hidden);
    };
    
    /**
     * Displays status message as SUCCESSFUL.
     *
     * @param       message     Status message to be displayed.
     * @returns     Messages.settings.target
     */
    self.success = function (message) {
        return self.show(message).addClass(self.settings.className.success);
    };
    
    /**
     * Displays status message as a WARNING.
     *
     * @param       message     Status message to be displayed.
     * @returns     Messages.settings.target
     */
    self.warning = function (message) {
        return self.show(message).addClass(self.settings.className.warning);
    };
    
    /**
     * Displays status message as an ERROR.
     *
     * @param       message     Status message to be displayed.
     * @returns     Messages.settings.target
     */
    self.error = function (message) {
        return self.show(message).addClass(self.settings.className.error);
    };
    
    return self;
}());