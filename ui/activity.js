module.exports = (function () {

    'use strict';
        
    /**
     * Page Activity For Session Expire
     */
    var self = {}, $;

    /**
     * Initializes the Activity Object
     *
     * @param       options     Settings.
     */
    self.init = function (_$, options) {
        $ = _$;

        self.settings = {
            target:  '.userAction',
            
        };

        $.extend(self.settings, options);

        $(document).ready(function() {  
            if ($(document).find(self.settings.target).length > 0)
            {
                self.activity();
            }
        });  

        return self;
    };
    
    /**
     *  Hides the status message.
     */
    self.activity = function () {

        //setup ajax error handling
        $.ajaxSetup({
            error: function (x, status, error) {
                if (x.status == 403) {
                    window.location.href ="/session/expired";
                }
                else {
                }
            }
        });

        var activityTimer = setInterval(function() {
            $.get('/check/activity', function(data) {
                if (parseInt(data) === 0 && $(document).find(self.settings.target).length > 0) {
                    clearInterval(activityTimer);
                    location.href = '/session/expired';
                }
            });
        }, 302000);
    };
    
    return self;
}());