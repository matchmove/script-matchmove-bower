module.exports = (function(){

    'use strict';

    /**
     * Requires:
     *
     *      $           jQuery library
     */
        
    /**
     * Public Siderbar Navigation
     *
     */
    var self = {}, $;

     /**
     * Initializes the Sidebar Navigation Object
     *
     * @param       options     Settings.
     */
    self.init = function (_$, options) {
        $ = _$;
        
        self.settings = {
            target:  $('body'),
            selectors: {
                nav: '#cd-lateral-nav',
                trigger: '#cd-menu-trigger',
                wrapper: '.cd-main-content',
                accordion: '.js_Accordion',
                accordion_row: 'js_Accordion_row',
                children: '.item-has-children',
                submenu: '.sub-menu'
            },
            
            className: {
                open: 'lateral-menu-is-open',
                one: 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
                hidden: 'displayNone',
                clicked: 'is-clicked',
                overflowhidden: 'overflow-hidden',
                display_active: 'display-active',
                display_hidden: 'display-hidden',
                minus_circle: 'fa-minus-circle',
                plus_circle: 'fa-plus-circle',
                submenu: 'submenu-open'
            },

            is_explorer_9: navigator.userAgent.indexOf('MSIE 9') > -1
            
        };
    
        $.extend(self.settings, options);
        self.toggle();

        return self;
    };

    /**
     *  toggleClass for Sidebard display
     */
    self.toggle = function () {
        var settings = self.settings;
        $(document).ready(function($) {

            //open-close lateral menu clicking on the menu icon
            $(settings.selectors.trigger).on('click', function(event) {

                event.preventDefault();

                $(this).toggleClass(settings.className.clicked);
                $(settings.selectors.wrapper).toggleClass(settings.className.open).one(settings.className.one, function() {
                    // firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
                    settings.target.toggleClass(settings.className.overflowhidden);
                });
                $(settings.selectors.nav).toggleClass(settings.className.open);
                if (settings.is_explorer_9) {
                    settings.target.toggleClass(settings.className.overflowhidden);
                }
            });

            //close lateral menu clicking outside the menu itself
            $(settings.selectors.wrapper).on('click', function(event) {

                if (!$(event.target).is(settings.selectors.trigger +', '+settings.selectors.trigger+' span')) {
                    $(settings.selectors.trigger).removeClass(settings.className.clicked);
                    $(settings.selectors.wrapper).removeClass(settings.className.open).one(settings.className.one, function() {
                        // firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
                        settings.target.removeClass(settings.className.overflowhidden);
                    });
                    $(settings.selectors.nav).removeClass(settings.className.open);
                    if (settings.is_explorer_9) {
                        settings.target.removeClass(settings.className.overflowhidden);
                    }
                }
            });
            
            $(settings.selectors.children).children('a').on('click', function(event) {
                event.preventDefault();
                $(this).toggleClass(settings.className.submenu)
                    .next(settings.selectors.submenu).slideToggle(200).end()
                    .parent(settings.selectors.children).siblings(settings.selectors.children)
                    .children('a').removeClass(settings.className.submenu)
                    .next(settings.selectors.submenu).slideUp(200);
            });

            /**
            *    For Accordion Divs
            */
            $(settings.selectors.accordion).on('click',function(event){

                event.preventDefault();

                var el = $(this);
                var elparent = $(el).parent();

                if($(elparent).hasClass(settings.className.display_active)){
                    $(elparent).removeClass(settings.className.display_active).addClass(settings.className.display_hidden);
                    $(el).find('i').removeClass(settings.selectors.minus_circle).addClass(settings.selectors.plus_circle);
                    $(elparent).siblings(settings.selectors.accordion_row).removeClass(settings.className.display_active).addClass(settings.className.display_hidden);
                }
                else{
                    $(elparent).removeClass(settings.className.display_hidden).addClass(settings.className.display_active);
                    $(el).find('i').removeClass(settings.selectors.plus_circle).addClass(settings.selectors.minus_circle);
                    $(elparent).siblings(settings.selectors.accordion_row).removeClass(settings.className.display_active).addClass(settings.className.display_hidden);
                }

            });

        });
    };

    return self;
}());