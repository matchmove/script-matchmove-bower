module.exports = (function(){

    'use strict';

    /**
     * Requires:
     *
     *      $           jQuery library
     */

    /**
     * Public Settion Action Tab
     *
     */
    var self = {}, $;

     /**
     * Initializes the Action Tab Object
     *
     * @param       options     Settings.
     */
    self.init = function (_$, options) {
        $ = _$;

        self.settings = {
            target:  $('body'),
            targetHtml:  $('html'),
            outer: $('.section__outer'),
            actionList: $('.section__action li a'),

            selectors: {
                trigger: '.abs-trigger',
                table: '.triggertable'
            },

            className: {
                active: 'tab--section-is-active',
                row_active: 'row-active'
            }

        };

        $.extend(self.settings, options);

        self.toggle();

        return self;
    };

    /**
     *  toggleClass for Action Tab display
     */
    self.toggle = function () {
        var settings = self.settings;
        $(document).ready(function($) {
            if(settings.outer.length) {

                var actionList = settings.actionList;

                /**
                * toggle Action Tab display
                */
                $(settings.selectors.trigger).on('click',function(e){
                    if (e.target.type == 'checkbox') {
                        return ;
                    }
                    settings.target.toggleClass(settings.className.active);
                    settings.targetHtml.toggleClass(settings.className.active);
                });

                /**
                * for table row click toggle Action Tab display
                */
               
                $(settings.selectors.table).delegate('tr', 'click', function(e){
                    if (e.target.type == 'checkbox') {
                        return ;
                    }
                          
                    e.preventDefault();

                    var user_id = $(this).attr('data-user'),
                        transaction_id = $(this).attr('data-transaction'),
                        provider_code = $(this).attr('data-provider-code'),
                        consumer_key = $(this).attr('data-consumer'),
                        ref_id = $(this).attr('data-ref-id'),
                        status = $(this).attr('data-status'),
                        query_string = '',
                        appended_params = '?',
                        channel_id = $(this).attr('data-channel-id'),
                        fee_id = $(this).attr('data-fee-id');

                    //remove active class 
                    $.each( $(settings.selectors.table).find('>tbody>tr'), function( i, n ) {
                        $(n).removeClass(settings.className.row_active);
                    });

                    //add active class to current row
                    $(this).addClass(settings.className.row_active);

                    //trigger
                    settings.target.addClass(settings.className.active);
                    settings.targetHtml.addClass(settings.className.active);

                    //get all the query 
                    $.each(_getUrlParams(),function(i,n){
                        if(i.length>0 && i != 'user_id'){
                            if(i.search("consumer_key")=='-1' && i.search("ref_id")=='-1' && i.search("status")=='-1')
                            {
                               query_string += '&'+i+'='+n;
                            }
                            
                        }
                    });                

                    //change the link
                    if($(this).attr('data-user')>0){
                        appended_params += 'user_id='+user_id+'&';
                    }

                    if(typeof $(this).attr('data-transaction') !== typeof undefined && $(this).attr('data-transaction')!=0){
                        appended_params += 'transaction_id='+transaction_id;
                    } else if(typeof $(this).attr('data-channel-id') !== typeof undefined && $(this).attr('data-channel-id')!=0) {
                        appended_params += 'channel_id='+channel_id+'&fee_id='+fee_id;
                    } else
                    if(typeof $(this).attr('data-provider-code') !== typeof undefined && $(this).attr('data-provider-code')!=0){
                        appended_params += 'provider_code='+provider_code;
                    }else
                    if((typeof $(this).attr('data-consumer') !== typeof undefined && $(this).attr('data-consumer') !== false &&
                        typeof $(this).attr('data-status') !== typeof undefined && $(this).attr('data-status') !== false &&
                        typeof $(this).attr('data-ref-id') !== typeof undefined && $(this).attr('data-ref-id') !== false)) {
                        appended_params += 'consumer_key='+consumer_key+'&ref_id='+ref_id+'&status='+status;
                    }

                    if(appended_params != '?'){
                        $.each( $(actionList), function( i, n ) {
                            $(n).attr('href',$(n).attr('route')+appended_params+query_string);
                        });
                    }

                    // to prevent viewport jump on click event
                    return false;

                });

            }
        });
    };

    /**
     * get query string parse to array, for page load
     *
     * @returns     array params
     */
    var _getUrlParams = function(){

        var search = window.location.search ; 
        var tmparray = search.substr(1,search.length).split("&");
        var params = {}; 
        if( tmparray != null)
        {
            for(var i = 0;i<tmparray.length;i++)
            {
                var reg = /[=|^==]/; 
                var set1 = tmparray[i].replace(reg,'&');
                var tmpStr2 = set1.split('&');
                params[tmpStr2[0]] = tmpStr2[1];
            }
        }
        
        return params ;     
    }

    return self;
}());
