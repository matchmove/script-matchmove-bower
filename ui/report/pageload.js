module.exports = (function(){

    'use strict';

    /**
     * Requires:
     *
     *      $           jQuery library
     */

    /**
     * Use pagination footable jquery load the page
     * draw table,draw query form,
     * support browser button back/forward using jquery history
     */

    var self = {}, $;

    /**
     * Initializes the PageLoad Object
     *
     * @param       options     Settings.
     */
    self.init = function (_$, options) {
        $ = _$;

        self.settings = {

            page: '1',

            page_url: $('#hdn_page_url').val(),

            total_pages: $('#hdn_total_pages').val(),

            total_record: $('#hdn_total_count').val(),

            paginationHolder: $('.paginationHolder'),

            target:  $('.footable'),

            targetHead:  $('.footable>thead'),

            targetBody:  $('.footable>tbody'),

            targetFoot:  $('.footable>tfoot>tr'),

            processing:  $('.table_processing'),

            selectors: {
                text: '.media .media-body'
            },
            className: {
                hidden: 'displayNone'
            },
            select_all: {
                controll : null,
                selected_records: [],
                hidden_ref_ids : 'hidden_ref_ids'
            }

        };

        $.extend(self.settings, options);

        return self;
    };

    /**
     * User Jquery Load a page to footable,
     * load funcaiton param is an url
     * This function use for report pagination
     *
     * @returns     true
     */
    var _load = function(){

        var settings = self.settings;
        var url = settings.page_url+'?&'+'page='+settings.page;
        var mydatatable = settings.target.data('footable');
        var rowobj = settings.target.find('tbody > tr');

        //get all the query
        $.each(_getUrlParams(),function(i,n){
            if(i.length>0){
                url += '&'+i+'='+n;
            }
        });

        $.each(rowobj, function(i,n){
            mydatatable.removeRow(n);
        });

        if(settings.paginationHolder.size()>0){
            settings.paginationHolder.pagination('drawPage',settings.page);
            _drawpage();
        }

        //load page and redraw footable
        $.get(url, function(response){

            settings.targetBody.html(response);

            self.drawcallback();

            self.selectable();
            settings.processing.hide();
            settings.processing.addClass('hidden');
            settings.paginationHolder.removeClass(settings.className.hidden);
            //window.Textarea.AutoresizeAttach();
        });

        return true;
    }

    /**
     * simple Pagenation
     *
     * @returns     true
     */
    var _pageclick = function(){

        var settings = self.settings;
        settings.paginationHolder.pagination({
            items       : settings.total_pages,
            nextText    : "›" ,
            prevText    : "‹" ,
            firstText   : '«',
            lastText    : '»',
            onPageClick : function (pageNumber, event) {
                var params = _getUrlParams();
                params.page = pageNumber;
            }
        });

        return true;
    }

    /**
     * get query string parse to array, for page load
     *
     * @returns     array params
     */
    var _getUrlParams = function(){

        var search = window.location.search ;
        var tmparray = search.substr(1,search.length).split("&");
        var params = {};
        if( tmparray != null)
        {
            for(var i = 0;i<tmparray.length;i++)
            {
                var reg = /[=|^==]/;
                var set1 = tmparray[i].replace(reg,'&');
                var tmpStr2 = set1.split('&');
                params[tmpStr2[0]] = tmpStr2[1];
            }
        }

        return params ;
    }

    /**
     * for history back/forward,  change the query form using query string  of the url
     *
     * @returns     true
     */
    var _drawpage = function(){
        $.each(_getUrlParams(),function(i,n){
            if(i.length>0){
                if($('select[name="'+i+'"]').size()>0){
                    $('select[name="'+i+'"]>option[value="'+n+'"]').prop('selected',"selected");
                    $('select[name="'+i+'"]>option:selected').scrollTop();
                }else
                    if($('input[name="'+i+'"]').size()>0){
                        if($('input[name="'+i+'"]').is( ":text" )){
                        	if(n != ''){
                                    $('input[name="'+i+'"]').val(decodeURIComponent((n+'').replace(/\+/g, '%20')));
                                }  
                        }else{
                            $('input[name="'+i+'"][value="'+n+'"]').prop("checked","1");
                        }
                    }
            }
        });
        return true;
    }

    /**
     * for those numbric data of the report,display number format
     *
     * @param       string nStr
     * @returns     string s
     */
    var _addCommas = function(nStr){

        var nStr = new Number(nStr);
        nStr = nStr.toFixed(3);
        nStr += '';
        var x = nStr.split('.'),
        x1 = x[0],
        x2 = x.length > 1 ? '.' + x[1] : '';

        if(x2.length>0){
            x2 = '0'+x2;
            var num = new Number(x2);
            num = num.toFixed(3);
            x = num.split('.');
            x2 = x.length > 1 ? '.' + x[1] : '';
        }
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        var s= x1 + x2;
        return s=='0'?'0':s;
    }

    var currency_formats = {
        SGD: {
            decimal_point: '.',
            group_separator: ',',
            grouping_end: '3',
            grouping_start: '3'
        },
        INR: {
            decimal_point: '.',
            group_separator: ',',
            grouping_end: '3',
            grouping_start: '2'
        },
        VND: {
            decimal_point: ',',
            group_separator: '.',
            grouping_end: '3',
            grouping_start: '3'
        },
        THB: {
            decimal_point: '.',
            group_separator: ',',
            grouping_end: '3',
            grouping_start: '3'
        },
        PHP: {
            decimal_point: '.',
            group_separator: ',',
            grouping_end: '3',
            grouping_start: '3'
        },
        IDR: {
            decimal_point: '.',
            group_separator: ',',
            grouping_end: '3',
            grouping_start: '3'
        },
        USD: {
            decimal_point: '.',
            group_separator: ',',
            grouping_end: '3',
            grouping_start: '3'
        }
    };

    /**
     * checks if the number string is a float based on currency format decimal point
     *
     * @param       string nStr
     * @param       string format
     * @returns     bool
     */
    var _isFloat = function(nStr, format){
        if(!isNaN(parseFloat(nStr)) && nStr.toString().indexOf(currency_formats[format].decimal_point) != -1)
        {
            return true;
        }
        return false;
    }

    /**
     * parse the number string as float based on currency format
     *
     * @param       string num
     * @param       string format
     * @returns     float
     */
    var _parseFloat = function(num, format){
        var group_separator_regexp = new RegExp('\\'+currency_formats[format].group_separator, 'g');
        num = num.replace(group_separator_regexp, '');
        if(currency_formats[format].decimal_point != '.')
        {
            var decimal_point_regexp = new RegExp(currency_formats[format].decimal_point, 'g');
            num = num.replace(decimal_point_regexp, '.');
        }
        return parseFloat(num);
    }

    /**
     * format number depending on currency
     *
     * @param       float num
     * @param       string format
     * @returns     string
     */
    var _currencyFormat = function(num, format){
        num = num.toString();
        var after_decimal_point = '';
        var decimal_point_index = num.indexOf('.');
        if(decimal_point_index > 0)
        {
           after_decimal_point = num.substring(decimal_point_index, num.length);
        }
        num = Math.floor(num);
        num = num.toString();
        var last_group = num.substring(num.length - currency_formats[format].grouping_end);
        var other_groups = num.substring(0, num.length - currency_formats[format].grouping_end);
        if(other_groups != '')
        {
            last_group = currency_formats[format].group_separator + last_group;
        }
        var grouping_start_regexp = new RegExp('\\B(?=(\\d{'+currency_formats[format].grouping_start+'})+(?!\\d))', 'g');
        return other_groups.replace(grouping_start_regexp, currency_formats[format].group_separator) + last_group + after_decimal_point.replace(/\./g, currency_formats[format].decimal_point);
    }

    /**
     * calculate the total column for report table after data loaded
     *
     * @returns     string s
     */
    self.drawcallback = function(){

        var settings = self.settings;
        var $tbody = settings.target.find('tbody'),
            $tfoot = settings.target.find('tfoot > tr'),
            $rows = $tbody.find('> tr:visible'),
            $firstrow = $rows.eq(0).find(' > td, > td.subhead tbody > tr > td'),
            rowsize = $rows.size(),
            number_string = '',
            number_float = 0,
            is_float = false,
            total = 0,
            ntotal = 0,
            columns = $firstrow.size(),
            $tfoot_total,
            $tfoot_total_precision,
            $tfoot_total_currency;

        if($tfoot.size()<1){
            return false;
        }
        if(rowsize<1){
            $tfoot.hide();
            return false;
        } else {
            $tfoot.show();
        }
        $tfoot.removeClass(settings.className.hidden);
        for(var t=0;t<columns;t++){
            if($firstrow.eq(t).hasClass('alignRight')){
                total = 0;
                is_float = false;

                $tfoot_total = $tfoot.find(' > td, > td.subfoot tbody > tr > td').eq(t);
                $tfoot_total_precision = $tfoot_total.attr('data-precision');
                $tfoot_total_currency = $tfoot_total.attr('data-currency');

                if(typeof $tfoot_total_precision == typeof undefined || $tfoot_total_precision == false){
                    $tfoot_total_precision = 2;
                }

                if(typeof $tfoot_total_currency == typeof undefined || $tfoot_total_currency == false){
                    $tfoot_total_currency = 'SGD';
                }

                for(var r=0;r<rowsize;r++){
                    $.each($rows.eq(r).find('>td, > td.subhead tbody > tr > td').eq(t), function(i,n){
                        number_string = $(n).text();
                        if(number_string != ''){
                            if(!is_float)
                            {
                                is_float = _isFloat(number_string, $tfoot_total_currency);
                            }
                            number_float = _parseFloat(number_string, $tfoot_total_currency);
                            if(isNaN(number_float)) return;
                            if($(n).hasClass('total')){
                                total = number_float > total ? number_float : total;
                            }else{
                                total = total + number_float;
                            }
                        }
                    });
                }

                if(is_float){
                    total = parseFloat(total).toFixed($tfoot_total_precision);
                }

                ntotal = _currencyFormat(total, $tfoot_total_currency);
                $tfoot_total.text(ntotal);
            }
        }
    }

    /**
     * history init ,save the page to history, to support back/forward buttons
     *
     *
     */
    self.pageload = function(){

        var settings = self.settings;
        if(settings.paginationHolder.size()>0){
            _pageclick();
        }


        if((settings.target.hasClass('multicard') && settings.targetHead.size() > 0) ||  settings.targetBody.size() > 0 ){

            $.history.init(function(url) {
                if(settings.total_record>0){
                    settings.processing.show();
                    settings.processing.removeClass('hidden');

                    settings.page = url == "" ? "1" : url;

                    //get hash then get number
                    settings.page = parseInt(settings.page.replace(/^.*(page-)/, ''));

                    if(settings.page<1 || parseInt(settings.page)>parseInt(settings.total_pages)){

                        settings.page = 1;
                    }
                    _load();
                }
            });
        }

    };

    /**
     * Add checkboxes dynamically to allow individual record selection
     *
     */
    self.selectable = function(){
        var settings = self.settings;
        var $tbody = settings.target.find('tbody'),
            $header = settings.target.find('thead > tr'),
            $firstHead = $header.find(' > th').eq(0),
            $rows = $tbody.find('> tr:visible'),
            $header = $tbody.find('> tr:visible'),
            $firstrow = $rows.eq(0).find(' > td, > td.subhead tbody > tr > td'),
            rowsize = $rows.size(),
            columns = $firstrow.size();
        if( settings.target.hasClass('selectable-records') && ( settings.targetHead.size() > 0  || settings.targetBody.size() > 0 ) ){
            // Add checkbox to other rows
            $rows.each(function(){
                var current = $(this),
                    ref_id = current.data('ref-id'),
                    first_td = current.find(' > td').first(),
                    checkbox = null;
                checkbox = $('<input type="checkbox" class="select_record" value="'+ref_id+'" />');
                first_td.prepend(checkbox);
                //  check if already selected
                if($.inArray(ref_id, settings.select_all.selected_records) != -1){
                    checkbox.prop('checked', true);
                }
            });

            // Add checkbox to first header
            // Check if already added
            if($firstHead.find('input:checkbox').size() <= 0){
                settings.select_all.controll = $('<input type="checkbox" class="select_all_records" />');
                $firstHead.prepend(settings.select_all.controll);

                //  Attach listener
                settings.select_all.controll.on('click', function(e){
                    var st = $(this).prop('checked');
                    $tbody.find(' > tr > td > input:checkbox').each(function(){
                        var ct = $(this).prop('checked', st).trigger('change');
                    });
                });
            }

            // Attach click listener to the checkboxes
            settings.target.find('tbody').find('input.select_record').on('change', function(){
                var current = $(this),
                    ref_id = current.val();
                if(current.prop('checked')){
                    settings.select_all.selected_records.push(ref_id);
                }
                else{
                    var _searchedIndex = $.inArray(ref_id, settings.select_all.selected_records);
                    if(_searchedIndex >= 0){
                        settings.select_all.selected_records.splice(_searchedIndex,1);
                    }
                }

                $('input[name="'+settings.select_all.hidden_ref_ids+'"]').val(settings.select_all.selected_records.join(','));
            });
        }

    };

    return self;
// Pull in jQuery and Underscore
})();
