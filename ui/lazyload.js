module.exports = (function(){

    'use strict';
    /**
     * Requires:
     *
     *      $           jQuery library
     */
        
    /**
     * lazy load items
     */
    var self = {}, $;

     /**
     * Initializes the LazyLoad Object
     *
     * @param       options     Settings.
     */
    self.init = function (_$, options) {
        $ = _$;
        
        self.settings = {
            scrollPos: 0,
            scrollProcessing: false,
            scrollCleanUp: false,
            target: $(document),
            scrollLoader: $('#js_scrollLoad'),
            link: '/accounts/manage/loadmore',
            scrollelem: '<div class="displayNone horLoader" id="js_scrollLoad"><i class="loader"></i></div>',
            item: $('#js_userItem'),

            
        };
    
        $.extend(self.settings, options);

        self.settings.item.parent().append(self.settings.scrollelem);

        return self;
    };

    /**
     * Query And Load
     *
     * @param       callback     Funcation.
     */
    function _execute(callback){

        var settings = self.settings;

        var direction = settings.target.scrollTop() - settings.scrollPos;

         settings.scrollPos = settings.target.scrollTop();

         if (direction <= 0 || settings.scrollProcessing || settings.scrollCleanUp) {
             return true;
         }

         var percentage = ((settings.target.scrollTop() + $(window).height()) / settings.target.height()) * 100;

         if (percentage > 90) {

             settings.scrollProcessing = true;
             settings.scrollLoader.removeClass('displayNone');
             $.ajax({
                 url: settings.link,
                 type: 'GET',
                 cache: false,
                 data: {
                     offset: settings.item.find('li').length,
                     username: urlParam('username')
                 },
                 dataType: 'json',
                 complete: function(data) {
                     settings.scrollProcessing = false;
                 },
                 success: function(data) {

                     if (data && data.length) {

                         for (var i = 0, length = data.length; i < length; i++) {

                             var item_promotion = settings.item.find('li').clone();

                             var view_details = item_promotion.find('.list-action>a.btn-green');
                             var delete_user = item_promotion.find('.list-action>a.js_linkUserDelete');
                             var resend_email = item_promotion.find('.list-action>a.js_linkEmailResend');

                             var admin_text = "";
                             if(typeof(data[i].is_admin) != "undefined" && data[i].is_admin) admin_text = " <b>(admin)</b>";
                             
                             item_promotion.find('h3').html(data[i].first_name +' '+ data[i].last_name+admin_text);
                             item_promotion.find('.info>p').text(data[i].email);

                             view_details.attr('href',view_details.attr('href').replace(view_details.attr('data-hash'),data[i].hash_id));
                             delete_user.attr('data-href',delete_user.attr('data-href').replace(view_details.attr('data-hash'),data[i].hash_id));
                             
                             if(data[i].is_active!=1)
                             {
                                 resend_email.show();
                             } 
                             
                             item_promotion.find('a').attr('data-hash',data[i].hash_id);
                             
                             settings.item.append(item_promotion[0].outerHTML);

                         }
                         settings.scrollLoader.addClass('displayNone');

                     } else {

                         settings.scrollCleanUp = true;
                         settings.scrollLoader.find('i').remove().html('Check Back Later for more Promotions');

                     }

                 },
                 error: function(data) {

                     settings.scrollLoader.find('i').remove().html('We are having trouble retriving the latest promotions. Kindly try again later');

                 }
             });
         }

    }

    /*
     * Get url param values
     */
    
    function urlParam(name){
    	
    	var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    	if (!results) { return ''; }
    	return decodeURIComponent(results[1]) || 0;
    	
    }       
    
    self.execute = function(callback){

        _execute(callback);
        
    };

    return self;
}());
